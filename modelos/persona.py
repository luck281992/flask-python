from config import db

class Persona(db.Model):
  __tablename__ = 'persona'
  id = db.Column('id', db.Integer, primary_key=True)
  nombre = db.Column('nombre', db.String(100), nullable=False)
  fecha_nacimiento = db.Column('fecha_nacimiento', db.DateTime, nullable=True)
  puesto = db.Column('puesto', db.String(100), nullable=False)

  def __init__(self, nombre, fecha_nacimiento, puesto):
  	self.nombre = nombre
  	self.fecha_nacimiento = fecha_nacimiento
  	self.puesto = puesto