#pip3 install flask_marshmallow
from flask_marshmallow import Marshmallow
from config import app

ma = Marshmallow(app)
class PersonaSchema(ma.Schema):
	class Meta: 
		fields = ("id", "nombre", "fecha_nacimiento", "puesto")