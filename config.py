#pip3 install flask-jwt-extended
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

app = Flask(__name__)
# Configuración base de datos
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:123@localhost/examen'
db = SQLAlchemy(app)

#Si se autehtifican por las cookies o ingresando el token cambiar a query_string
app.config['JWT_TOKEN_LOCATION'] = ['cookies']
app.config['JWT_CSRF_IN_COOKIES'] = True
app.config['JWT_CSRF_CHECK_FORM'] = True
app.config['JWT_QUERY_STRING_NAME'] = 'token'
app.config['JWT_ERROR_MESSAGE_KEY'] = "mensaje"
app.config['JWT_SECRET_KEY'] = 'password'
jwt = JWTManager(app)
identidad_token = 'Usuario publico'
