from flask import Flask, jsonify, request, render_template
from config import db, app
from datetime import datetime
from modelos.persona import Persona
from schemas.persona import PersonaSchema
from formularios.persona import PersonaForm
from flask_jwt_extended import jwt_required, jwt_optional, get_raw_jwt, get_jwt_identity

persona_schema = PersonaSchema()
personas_schema = PersonaSchema(many=True)

@app.route('/crear_tabla')
def crear_tabla():
	db.create_all()
	db.session.commit()
	return 'Se han creado tablas'

@app.route('/edit', defaults={"id": ""}, methods=['GET'])
@app.route('/edit/<int:id>', methods=['GET'])
@jwt_required
def edit(id):
	autentificado = get_jwt_identity()
	try:
		if request.method != 'GET':
			return jsonify(autentificado=autentificado, mensaje='El método HTTP debe ser GET'), 404

		if not id:
			id = request.args.get('id')

		if not id:
			return jsonify(autentificado=autentificado, mensaje='Parametro id invalido o vacio'), 400

		form = PersonaForm(request.form)
		persona = Persona.query.get(id)
		persona.fecha_nacimiento = persona.fecha_nacimiento.strftime('%Y-%m-%d')
		resultado = persona_schema.dump(persona)
		return render_template('persona.html', form=form, persona=resultado, autentificado=autentificado, csrf_token=(get_raw_jwt() or {}).get("csrf"))
	except Exception as e:
		return jsonify(autentificado=autentificado, mensaje='Error al cargar template'), 400

@app.route('/', methods=['GET'])
@jwt_optional
def view():
	autentificado = get_jwt_identity()
	try:
		if request.method != 'GET':
			return jsonify(autentificado=autentificado, mensaje='El método HTTP debe ser GET'), 404

		personas = Persona.query.all()
		resultado = personas_schema.dump(personas)
		return render_template('consulta.html', personas=resultado, autentificado=autentificado)
	except Exception as e:
		return jsonify(autentificado=autentificado, mensaje='Error al cargar template'), 400

@app.route('/add', methods=['GET'])
@jwt_required
def add():
	autentificado = get_jwt_identity()
	try:
		if request.method != 'GET':
			return jsonify(autentificado=autentificado, mensaje='El método HTTP debe ser GET'), 404

		form = PersonaForm(request.form)
		return render_template('persona.html', form=form, persona={}, autentificado=autentificado, csrf_token=(get_raw_jwt() or {}).get("csrf"))
	except Exception as e:
		return jsonify(autentificado=autentificado, mensaje='Error al cargar template'), 400

# Se guarda o actualiza registro de persona
@app.route('/save', defaults={"id": ""}, methods=['GET', 'POST'])
@app.route('/save/<int:id>', methods=['GET', 'POST', 'PUT'])
@jwt_required
def save(id):
	autentificado = get_jwt_identity()
	try:
		#El metodo GET se utiliza unicamente para el token y para obtener id al actualizar registro
		if request.method == 'GET':
			return jsonify(autentificado=autentificado, mensaje='El método HTTP debe ser POST o PUT'), 404
		#Se obtienen los datos ya sea por el form, json con el metodo POST
		if request.form:
			if not id and request.form.get('id'):
				id = request.form.get('id')

			nombre = request.form.get('nombre')
			fecha_nacimiento = datetime.strptime(request.form.get('fecha_nacimiento'), '%Y-%m-%d').date()
			puesto = request.form.get('puesto')
		elif request.json:
			if not id and request.json.get('id'):
				id = request.json.get('id')

			nombre = request.json.get('nombre')
			fecha_nacimiento = datetime.strptime(request.json.get('fecha_nacimiento'), '%Y-%m-%d').date()
			puesto = request.json.get('puesto')
		elif request.args:
			if not id and request.args.get('id'):
				id = request.args.get('id')

			nombre = request.args.get('nombre')
			fecha_nacimiento = datetime.strptime(request.args.get('fecha_nacimiento'), '%Y-%m-%d').date()
			puesto = request.args.get('puesto')

		if not nombre:
			return jsonify(autentificado=autentificado, mensaje='Nombre vacio'), 400
		if not fecha_nacimiento:
			return jsonify(autentificado=autentificado, mensaje='Fecha de nacimiento vacio'), 400
		if not puesto:
			return jsonify(autentificado=autentificado, mensaje='Puesto vacio'), 400

		if not id:
			persona = Persona(nombre, fecha_nacimiento, puesto)
			db.session.add(persona)
		else:
			persona = Persona.query.get(id)
			persona.nombre = nombre
			persona.fecha_nacimiento = fecha_nacimiento
			persona.puesto = puesto

		db.session.commit()
		resultado = persona_schema.dump(persona)
		return jsonify(autentificado=autentificado, mensaje='Se ha guardado persona', persona=resultado), 200
	except ValueError as v:
		return jsonify(autentificado=autentificado, mensaje='Valor de datos invalidos '+ v.args[0]), 400
	except Exception as e:
		return jsonify(autentificado=autentificado, mensaje='Parametros invalidos ' + e.args[0], estructura='nombre|fecha_nacimiento|puesto'), 400

# Regresa todas las personas
@app.route('/personas', methods=['GET'])
@jwt_required
def getItems():
	autentificado = get_jwt_identity()
	try:
		if request.method != 'GET':
			return jsonify(autentificado=autentificado, mensaje='El método HTTP debe ser GET'), 404

		personas = Persona.query.all()
		resultado = personas_schema.dump(personas)
		return jsonify(resultado), 200
	except Exception as e:
		return jsonify(autentificado=autentificado, mensaje='Error al obtener registros de personas ' + e.args[0]), 400

# Regresa una persona por medio del id
@app.route('/persona', defaults={"id": ""}, methods=['GET'])
@app.route('/persona/<int:id>', methods=['GET'])
@jwt_required
def getItem(id):
	autentificado = get_jwt_identity()
	try:
		if request.method != 'GET':
			return jsonify(autentificado=autentificado, mensaje='El método HTTP debe ser GET'), 404

		if not id:
			id = request.args.get('id')

		if not id:
			return jsonify(autentificado=autentificado, mensaje='Parametro id invalido o vacio'), 400

		persona = Persona.query.get(id)
		resultado = persona_schema.dump(persona)
		return jsonify(resultado), 200
	except Exception as e:
		return jsonify(autentificado=autentificado, mensaje='Error al obtener registro de persona'), 400

# Elimina una persona por medio del id
@app.route('/delete', defaults={"id": ""}, methods=["GET", "DELETE"])
@app.route('/delete/<int:id>', methods=["GET", "DELETE"])
@jwt_required
def delete(id):
	autentificado = get_jwt_identity()
	try:
		if not id:
			id = request.args.get('id')

		if not id:
			return jsonify(autentificado=autentificado, mensaje='Parametro id invalido o vacio'), 400

		persona = Persona.query.get(id)
		resultado = persona_schema.dump(persona)
		db.session.delete(persona)
		db.session.commit()
		return jsonify(autentificado=autentificado, mensaje='Se ha eliminado persona', persona=resultado), 200
	except Exception as e:
		return jsonify(autentificado=jwt_identidad, mensaje='Error al intentar eliminar persona ' + e.args[0]), 400