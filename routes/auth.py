from flask import jsonify, request
from config import app, identidad_token
from flask_jwt_extended import jwt_required, unset_jwt_cookies, set_access_cookies, set_refresh_cookies, get_csrf_token, create_refresh_token, jwt_optional, get_raw_jwt, get_jwt_identity, create_access_token

# Autentificación con cookies
@app.route('/login', methods=['GET'])
def auth():
  try:
    if request.method != 'GET':
      return jsonify(mensaje='El método HTTP debe ser GET'), 404

    app.config['JWT_TOKEN_LOCATION'] = ['cookies']
    token = create_access_token(identity=identidad_token)
    refresh_token = create_refresh_token(identity=identidad_token)
    respuesta = jsonify(access_csrf=get_csrf_token(token), refresh_csrf=get_csrf_token(refresh_token))
    set_access_cookies(respuesta, token)
    set_refresh_cookies(respuesta, refresh_token)
    return respuesta, 200
  except Exception as e:
  	return  jsonify(mensaje='Error al intentar authenticarse'), 400

# Autentificación con token de cadena
@app.route('/autentificado_token', methods=['GET'])
def autentificado_token():
  try:
    if request.method != 'GET':
      return jsonify(mensaje='El método HTTP debe ser GET'), 404

    app.config['JWT_TOKEN_LOCATION'] = ['query_string']
    token = create_access_token(identity=identidad_token)
    return  jsonify(token=token), 200
  except Exception as e:
    return  jsonify(mensaje='Error al intentar obtener el token'), 400

# Salir autentificación
@app.route('/logout', methods=['GET'])
@jwt_required
def logout():
	autentificado = get_jwt_identity()
	try:
		if request.method != 'GET':
			return jsonify(autentificado=autentificado, mensaje='El método HTTP debe ser GET'), 404

		respuesta = jsonify(autentificado=autentificado, logeado=False)
		unset_jwt_cookies(respuesta)
		return respuesta, 200
	except Exception as e:
		return  jsonify(mensaje='Error al intentar salir'), 400