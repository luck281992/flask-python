#sudo apt install python3-pip
#pip3 install flask 
from config import app
from gevent import pywsgi
import routes.persona
import routes.auth

if __name__ == "__main__":
	#app.run(debug = True)
	http_server = pywsgi.WSGIServer(('', 5000), app)
	http_server.serve_forever()
