#pip3 install wtforms
from wtforms import Form, IntegerField, StringField, SubmitField
from wtforms.validators import DataRequired
from wtforms.fields.html5 import DateField

class PersonaForm(Form):
	id = IntegerField('id')
	nombre = StringField('Nombre', validators=[DataRequired()])
	fecha_nacimiento = DateField('Fecha de nacimiento', format='%Y-%m-%d')
	puesto = StringField('Puesto', validators=[DataRequired()])
	submit = SubmitField('Guardar')

